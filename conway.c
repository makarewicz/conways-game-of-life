#include "stdio.h"
#include "stdlib.h"
#define WYSOKOSC 20
#define SZEROKOSC 20

extern conway_step(char board[WYSOKOSC][SZEROKOSC], int32_t height, int32_t width);

int read_board(char board[WYSOKOSC][SZEROKOSC], FILE* input) {
    int i, j;
    char *buf, *p;
    p = board[0];
    buf = (char*)calloc(SZEROKOSC + 2, sizeof(char));
    for (i = 0; i < WYSOKOSC; ++i) {
        fgets(buf, SZEROKOSC + 2, input);
        for (j = 0; j < SZEROKOSC; ++j) {
            *p = (buf[j] == '1') ? 1 : 0;
            ++p;
        }
    } 
    return 0;
}

int write_board(char board[WYSOKOSC][SZEROKOSC], FILE* output) {
    int i, j;
    for (i = 0; i < WYSOKOSC; ++i) {
        for(j = 0; j < SZEROKOSC; ++j) {
            putchar(board[i][j] ? '1' : '.');
        }
        putchar('\n');
    }
    return 0;
}

//extern void conway_step(size_t height, size_t width, char* board);
int main(int argc, char* argv[]) {
    char board[WYSOKOSC][SZEROKOSC];
    int i;
    FILE* input;

    if (argc < 2) {
        input = stdin;
    } else {
        input = fopen(argv[1], "r");
    }

    read_board(board, input);
    for (i = 0; i < 10; ++i) {
        conway_step(board, WYSOKOSC, SZEROKOSC);
        write_board(board, stdout);
        puts("");
    }
    return 0;
}
