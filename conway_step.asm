global conway_step
extern printf

section .data
    string db 'lets test %d',0xa,0

section .text
conway_step:
    enter 0, 0
    push ebx
    push esi
    push edi
read_arguments:
    mov esi, [ebp+8] ;board
    mov ebx, [ebp+12] ;height
    mov ecx, [ebp+16] ;width

create_temporary_row:
    sub esp, ecx ; allocating memory for temporary row
    mov edi, esp ; edi points on temporary row

    pushf
    cld 
    push ecx
    push edi
    mov eax, 0
    rep stosb ; initializing temporary row with 0s
    pop edi
    pop ecx
    popf

    mov edx, esi ; copying board ptr
    add edx, ecx ; moving that pointer to next row

;; Now: edi -> previous row (before modifing), esi -> current row,
;; edx -> next row
    push ebx
for_each_row:
    push edi ;saving pointer to temporary row
    push ecx ;saving height
    push dword 0 ;value of cell on left and up to the current
    mov eax, 0 ;eax holds sum of 8 adjacent cells
    add al, [edi]
    add al, [esi]
    add al, [edx]
for_each_column:
    cmp ecx, 1
    jz fc_after_adding
    add al, [edi+1]
    add al, [esi+1]
    cmp ebx, 1 ;skip if last row
    jz fc_after_adding
    add al, [edx+1] 
fc_after_adding:
    sub al, [esi] ;don't count current cell
    cmp al, 3
    jz fc_new_cell
    cmp al, 2
    jz fc_stay_same
    jmp fc_die
fc_new_cell:
    sub al, [esp] ;removes value of cell on left and up to the current
    add esp, 4
    push dword [edi] ;and keeps analogous value for the next column
    push eax
    mov al, [esi] 
    mov [edi], al ;stores current cell value in temporary row
    pop eax
    mov byte [esi], 1 ;sets cell to 1
    jmp after
fc_die:
    sub al, [esp] ;removes value of cell on left and up to the current
    add esp, 4
    push dword [edi] ;and keeps analogous value for the next column
    push eax
    mov al, [esi]
    mov [edi], al ;stores current cell value in temporary row
    pop eax
    mov byte [esi], 0 ;sets cell to 0
    jmp after
fc_stay_same:
    sub al, [esp] ;removes value of cell on left and up to the current
    add esp, 4
    push dword [edi] ;and keeps analogous value for the next column
    push eax
    mov al, [esi] ;stores current cell value in temporary row
    mov [edi], al
    pop eax
after:
    add al, [edi] ;adds previous value of current cell to eax
    cmp ecx, [ebp+16] ;skip removing previous column if this is the first one
    jz after_removing_prev_col
    sub al, [edi-1] ;we only remove values from current row, and the next one
    ;;since we removed value from previous row above
    cmp ebx, 1 ;skip if last row
    jz after_removing_prev_col
    sub al, [edx-1] 
after_removing_prev_col:
    dec ecx 
    
    inc edi ;iterating over previous, current and next rows
    inc edx
    inc esi
    cmp ecx, 0
    jnz for_each_column
    add esp, 4
    pop ecx
    pop edi
    dec ebx
    cmp ebx, 0

    jnz for_each_row
    pop ebx
    add esp, ebx ; freeing memory with temporary row
    pop edi
    pop esi
    pop ebx

    leave
    ret
