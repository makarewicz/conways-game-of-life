OUT = conway
CC = gcc
CFLAGS = -g -m32
ASM = nasm
AFLAGS = -f elf32 -g
ODIR = obj

all:$(OUT)

$(ODIR)/conway_step.o: conway_step.asm
	$(ASM) -o $@ $< $(AFLAGS) 

$(ODIR)/conway.o: conway.c
	$(CC) -c -o $@ $< $(CFLAGS)

$(OUT): $(ODIR)/conway.o $(ODIR)/conway_step.o
	$(CC) -o $@ $^  $(CFLAGS)

test: $(OUT)
	./runtests $(OUT) tests


clean:
	rm -f $(ODIR)/*.o $(OUT)
